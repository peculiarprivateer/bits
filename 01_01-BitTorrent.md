> The documentation in this repository is for educational purposes only. The 
> author and its associates do not condone the misuse of any of the listed 
> software programs to illegally acquire or distribute copyrighted content.

# BitTorrent
Wikipedia defines BitTorrent as a communication protocol for peer-to-peer file
sharing protocol which enables users to distribute data and electronic files
over the internet in a decentralized manner.

In other words, rather than downloading a complete file from a single host
server, the file is hosted by many individuals. As you download the file, it 
is split into chunks which may be independently delivered from the available 
seeders. Although availability and performance of a particular torrent can
vary widely, the primary benefit of the decentralized structure is reslience.
The download is not reliant on a single continuous stream of data from a
single website or server that can fail if your internet connection is slow
or unstable, or can become unavailable if the website ever goes down for any
reason. Because the content is delivered in chunks from many different hosts,
a download can run in the background as internet connections change, or as
individual seeds join or leave the swarm.

> [!TIP]
> A popular and legitimate use case for BitTorrent is as a more efficient
> download source for Linux distribution installers.

## Basic Vocabulary
- **Client** - A program that enables peer-to-peer filesharing via the
BitTorrent protocol
- **Torrent** - Depending on context, this could mean a few different things
  - As a verb, to download data via the BitTorrent protocol
	- As a noun, a `.torrent` metadata file
	- As a noun, the data files described by a `.torrent` metadata file
- **`.torrent` File** - A metadata file describing a file or group of files
to be shared. A `.torrent` file can be added in your Client program to begin
downloading the set of files it describes.
- **Magnet Link** - An alternative mechanism to a `.torrent` file for
identifying a torrent.
- **Peer** - One instance of a BitTorrent client which to which other clients
can connect and transfer data. In other words, the "downloader".
- **Seed** - A machine posessing all of the data for a given torrent. In other
words, the "uploader".
- **Swarm** - All of the peers and seeders charing a particular torrent.
- **Tracker** - A server which tracks the seeds and peers in a swarm. Clients
report their own status to the tracker, and receive information about the
status of other peers in the swarm. Fundamentally, this is how your client
knows where to upload or download data from.
- **Index** - A list of `.torrent` files managed by a website and available
for searches.

## Index Websites
There are a number of popular index websites. The first that many people have
heard of is called The Pirate Bay, but it is by no means the only, or even the
best torrent index site.

- The Pirate Bay
- LimeTorrents
- Kickass Torrents
- Torrent Galaxy
- TorrentProject
- Solid Torrents

### Proxy Hosts
Although the BitTorrent protocol is widely used for *completely legitimate*
filesharing purposes, unfortunately, it has developed a reputation as being
used for illegally sharing copyrighted media. As such, index websites that
increase accessibility to `.torrent` files and magnet links are also
associated with this reuptation, and are targeted for legal action by
copyright holders. Because of this, index websites themselves are also
deployed and hosted in a distributed manner to increase resilience when a
single host is sucessfully taken down. Any number of proxy hosts, which are
manifested as different publicly accessible URLs, are usually available for
each of the most popular indexing websites. Lists of the currently available
proxies can usually be found easily with a simple Google search.

### Private Indexes
In addition to the popular public indexes, there are a number of high-quality
private indexes. Private indexes are usually invitation-only, and require
certain qualifications to receive an invitation. At this time, I have not
quite cracked the code on how to break into this loop, but it's nice to know
that it exists.

### Safety Warning
Due to the relative ease and publicity of popular sites like The Pirate Bay,
it is not uncommon for there to be low-quality torrents or even torrents
that contain viruses or malware. Even the popularity of a torrent, indicated
by the number of seeds and peers, is not a guarantee of quality or safety.
At this time I don't have any particular advice on how to check the validity
of a torrent, other than "proceed with caution." In reality, it doesn't seem
to be a widespread issue, but it is nonetheless possible, and something to be
aware of.

## Clients
There are a number of popular clients available, but they all perform the same
basic function to connect to a torrent in order to transfer data.

- BitTorrent (confusingly, the name of the protocol in general, AND the name
of this particular client)
- qBittorrent
- Deluge
- μTorrent
- rTorrent
- etc

There are, of course torrent clients available for desktop enviornments, but
especially if you have a laptop, I would *not* advise torrenting on desktop.
Torrenting can require hours or days to complete, and while it is certainly
possible to pause and resume a torrent based on your current activity, it may
be difficult to maintain an appropriate ratio (see "Etiquette" below).

In my opinion, torrenting is much better suited for a server or a stationary
desktop workstation that usually stays on. Many torrent clients are available
as web apps, which makes management easier when installed on a headless 
server.

In my case, I use qBittorrent installed as a container on Unraid.

## How to Torrent
The basic setup for torrenting is quite straightforward:

1. Choose and install a torrenting client.
2. Configure your client to only connect via a proxy server (see "Security"
below).
3. Configure any other settings in your client, such as authentication
credentials, your preferred ratio limit (see "Etiquette" below), and your
download directory
4. Find a `.torrent` file or a magnet link by visiting one of the
afforementioned index websites, or directly from the vendor
  - e.g. https://ubuntu.com/download/alternative-downloads
5. Open your client, then add the `.torrent` file or magnet link
6. Wait patiently for your download to complete
7. COPY (do not move) the files from the download path to the final
destination
	- Moving the file will disrupt your client from seeding the data to other
peers
8. Wait patiently for the torrent to meet your ratio limit
9. Remove the torrent from your client, and the data from the original
download directory

### Etiquette
The success of a given torrent is much like the infection rates of a virus. It 
grows when each person that currently has passes it forward to others at a 
ratio greater than 1, and it dies off when it is passed forward at a ratio 
less than 1. A ratio of 1 means that you have uploaded as much data as you
have downloaded, a ratio of less than 1 means that you have downloaded more
than you have uploaded, and a ratio greater than 1 means that you have
uploaded more than you have downloaded.

It is generally considered good etiquette to have a share ratio greater than
1, (I usually target 1.1 or 1.2 when torrenting Linux distributions). In turn,
it is poor etiquette to have a share ratio less than 1, and it is considered
extremely poor etiquette, even *malicious* to have a ratio close to 0. In
other words, immediately removing a torrent from your client after you finish
downloading the content without uploading any yourself. This is commonly
called "leeching" or "hit-and-run", and it stresses the torrent network. Some
trackers will even kick users out for maintaining poor ratios.

Most clients allow you to configure a global ratio setting, which will
retain torrents until you reach the desired upload ratio, at which point it
will be automatically paused or removed.

## Security
Safety and security in the torrent network relies on as much anonymity as
possible. When browsing index sites always be sure to use a VPN in order to
protect your IP address. In addition, you should *only* use your torrent
client when connected to a VPN, or even better, configure your torrent client
to connect via a proxy server. Configuring your client directly with a proxy
is a good way to ensure that you never forget to turn on your VPN.

Especially when it comes to the client, the VPN or proxy connection
is particularly important for two reasons:
1. Because torrenting traffic has a very distinct pattern which is easy for 
Internet Service Providers to recognize. Through a complex web of legal finger
pointing, copyright holders pressure ISPs to aid in the enforcement of 
copyright law by sending "Cease and Desist" notices to customers with clear 
torrent traffic. However, using a proxy or VPN masks your traffic in a way 
that prevents ISPs from snooping. And of course, when you use your torrenting 
client to download Linux ISOs, you aren't violating copyright law, but that's 
not the point - the point is that how you use your internet connection is your 
business, and shouldn't be subject to judgement or restriction by a nosy ISP.
2. Because of the nature of the tracker servers which communicate and organize
traffic between seeds and peers in a swarm, your public IP is visible and
available to everybody on the torrent. Using a VPN or proxy masks your IP
from any malicious parties on the swarm who might target your public IP
address for attacks.

Again, it should be stressed that these security measures are not optional,
and should be configured in a way where it is impossible to "forget." Again,
configuring a proxy directly in your client's connection settings is one of
the easiest ways to accomplish this.
