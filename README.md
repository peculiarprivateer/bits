> The documentation in this repository is for educational purposes only. The 
> author and its associates do not condone the misuse of any of the listed 
> software programs to illegally acquire or distribute copyrighted content.

# Bits

## Prelude - Fun Resources
- [Sailor's Rite of Passage](https://www.reddit.com/r/Piracy/wiki/faq/#wiki_.1F4D1_.279C_rite_of_passage)
- [*Don't Download This Song* - Weird Al Yankovic](https://youtube.com/watch?v=zGM8PT1eAvY)

## Section 1 - Basic Tools
1. [BitTorrent](01_01-BitTorrent.md)
2. [Indexes and RSS Feeds](01_02-Indexes_and_RSS_Feeds.md)
3. [*Arrs](01_03-Arrs.md)
